# Build container
FROM node:lts-alpine AS build

WORKDIR /build/

# Install pnpm
RUN npm install -g pnpm

# Install packages
COPY package.json pnpm-lock.yaml /build/
RUN pnpm install

WORKDIR /build/dist
COPY package.json pnpm-lock.yaml /build/dist/
RUN pnpm install --prod

WORKDIR /build/

COPY ./ /build/
RUN pnpm run build

# App container
FROM node:lts-alpine

WORKDIR /app

COPY --from=build /build/dist /app

CMD node index.js
