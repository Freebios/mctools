export const fromHex = (hex: string) : string => {
  if (hex.length !== 32) throw new Error('Invalid UUID');
  const uuid = `${hex.substr(0, 8)}-${hex.substr(8, 4)}-${hex.substr(12, 4)}-${hex.substr(16, 4)}-${hex.substr(20, 12)}`;
  return uuid;
};

export const toHex = (uuid: string) : string => {
  if (uuid.length !== 36) throw new Error('Invalid UUID');
  return '';
};
