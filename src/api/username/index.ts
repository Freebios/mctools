import Router from '@koa/router';

import username2uuid from './username2uuid';
import whitelist from './whitelist';

const router = new Router();

router.use('/username2uuid', username2uuid.routes(), username2uuid.allowedMethods());
router.use('/whitelist', whitelist.routes(), whitelist.allowedMethods());

export default router;
