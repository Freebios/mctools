import Router from '@koa/router';
import { usernamesToUuids } from '../../mojangAPI';
import { fromHex } from '../../uuidUtils';

const router = new Router();

router.get('/:usernames', async (ctx, next) => {
  const usernames = ctx.params.usernames.split(' ');
  const profiles = await usernamesToUuids(usernames);

  const whitelist = [];
  for (let i = 0; i < profiles.length; i += 1) {
    whitelist.push({
      uuid: fromHex(profiles[i].uuid),
      name: profiles[i].username,
    });
  }

  ctx.body = whitelist;
});

export default router;
