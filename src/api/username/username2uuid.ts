import Router from '@koa/router';
import { usernamesToUuids } from '../../mojangAPI';

const router = new Router();

router.get('/:usernames', async (ctx, next) => {
  const usernames = ctx.params.usernames.split(' ');

  const delimiter = ctx.request.query.delimiter as string;

  ctx.body = (await usernamesToUuids(usernames)).map((profile) => profile.uuid).join(delimiter || ' ');
});

export default router;
