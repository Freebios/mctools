import Router from '@koa/router';

import username from './username';
import uuid from './uuid';

const router = new Router();

router.use('/username', username.routes(), username.allowedMethods());
router.use('/uuid', uuid.routes(), uuid.allowedMethods());

export default router;
