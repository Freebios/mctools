import Router from '@koa/router';

import uuid2username from './uuid2username';
import whitelist from './whitelist';

const router = new Router();

router.use('/uuid2username', uuid2username.routes(), uuid2username.allowedMethods());
router.use('/whitelist', whitelist.routes(), whitelist.allowedMethods());

export default router;
