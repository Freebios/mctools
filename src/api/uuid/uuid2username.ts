import Router from '@koa/router';
import { uuidsToUsernames } from '../../mojangAPI';

const router = new Router();

router.get('/:uuids', async (ctx, next) => {
  const uuids = ctx.params.uuids.split(' ');

  const delimiter = ctx.request.query.delimiter as string;

  ctx.body = (await uuidsToUsernames(uuids)).map((profile) => profile.username).join(delimiter || ' ');
});

export default router;
