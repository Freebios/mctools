import Router from '@koa/router';
import { uuidsToUsernames } from '../../mojangAPI';
import { fromHex } from '../../uuidUtils';

const router = new Router();

router.get('/:uuids', async (ctx, next) => {
  const uuids = ctx.params.uuids.split(' ');
  const usernames = await uuidsToUsernames(uuids);

  const whitelist = [];
  for (let i = 0; i < uuids.length; i += 1) {
    whitelist.push({
      uuid: fromHex(uuids[i]),
      username: usernames[i],
    });
  }

  ctx.body = whitelist;
});

export default router;
