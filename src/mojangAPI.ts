import axios from 'axios';
import { fromHex } from './uuidUtils';

export const uuidsToUsernames = async (uuids: Array<String>) : Promise<any[]> => {
  const promises = [];
  for (let i = 0; i < uuids.length; i += 1) {
    promises.push(axios.get(`https://api.mojang.com/user/profiles/${uuids[i]}/names`));
  }

  const results = await Promise.allSettled(promises);

  const profiles = [];

  for (let i = 0; i < results.length; i += 1) {
    const result = results[i];

    if (result.status === 'fulfilled' && result.value.status === 200) {
      const { data } = result.value;
      profiles.push({
        uuid: uuids[i],
        username: data[data.length - 1].name,
      });
    } else {
      profiles.push({
        uuid: uuids[i],
        username: '',
        error: true,
      });
    }
  }

  return profiles;
};

export const usernamesToUuids = async (usernames: Array<String>) : Promise<any[]> => {
  const filteredUsernames = usernames.filter((username) => username.length > 0);
  const usernamesChunks : Array<Array<String>> = [];

  for (let i = 0; i < filteredUsernames.length; i += 10) {
    usernamesChunks.push(filteredUsernames.slice(i, i + 10));
  }

  const promises = [];
  for (let i = 0; i < usernamesChunks.length; i += 1) {
    promises.push(axios.post('https://api.mojang.com/profiles/minecraft', usernamesChunks[i]));
  }

  const results = await Promise.allSettled(promises);

  const profiles : Array<any> = [];

  for (let i = 0; i < results.length; i += 1) {
    const result = results[i];

    if (result.status === 'fulfilled' && result.value.status === 200) {
      let { data } = result.value;

      data = data.map((profile : any) => ({
        uuid: fromHex(profile.id),
        username: profile.name,
      }));

      profiles.push(...data);
    }
  }

  return profiles;
};
