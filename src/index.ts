import 'module-alias/register';

import Koa from 'koa';
import api from './api';

const app = new Koa();

app.listen(8080);

app.use(api.routes());
app.use(api.allowedMethods());
